from typing import List, Optional
import uuid
from sqlalchemy import delete, null, asc
from sqlalchemy.orm import Session

import base64

import schemas
import classes


# Get users article
def get_Article(db: Session, article_id: uuid, user: classes.Login):
    ret = (
        db.query(classes.Article.id, classes.Article.name, classes.Article.price, classes.Article.user, classes.Article.color)
        .filter(classes.Article.id == article_id and classes.Article.user == user.id)
        .first()
    )
    return ret

# Update the fields of a user's article
def update_Article(db: Session, article: schemas.Article, user: classes.Login):
    editArticle = (
        db.query(classes.Article)
        .filter(classes.Article.id == article.articleId and classes.Article.user == user.id)
        .first()
    )
    editArticle.name = article.name
    editArticle.price = article.price
    editArticle.color = article.color
    db.merge(editArticle)
    db.commit()
    return id

# Updates the image of a users article
def update_Article_image(db: Session, id: uuid.UUID, image: bytearray, user: classes.Login):
    editArticle = (
        db.query(classes.Article)
        .filter(classes.Article.id == id and classes.Article.user == user.id)
        .first()
    )
    editArticle.picture = image
    db.merge(editArticle)
    db.commit()
    return id

# Creates an article with some fields
def create_Article(db: Session, article: schemas.ArticleBase, user: classes.Login):
    id = uuid.uuid4()
    newArticle = classes.Article()
    newArticle.id = id
    newArticle.name = article.name
    newArticle.price = article.price
    newArticle.color = article.color
    newArticle.user = user.id
    db.add(newArticle)
    db.commit()
    create_Data(db, id, classes.MerchData(), user)
    return id

# Deletes the article
def delete_Article(db: Session, art_id: uuid, user: classes.Login):
    art = (
        db.query(classes.Article)
        .filter(classes.Article.id == art_id and classes.Article.user == user.id)
        .first()
    )
    return db.delete(art)


# Gets all article objects with their fields(without picture) of the user
def get_Articles(db: Session, user: classes.Login):
    ret = db.query(classes.Article.id, classes.Article.name, classes.Article.price, classes.Article.user, classes.Article.color).filter(classes.Article.user == user.id).all()
    return ret

# Gets the image of the article of the user
def get_Image_Article(db: Session, id: uuid.UUID, user: classes.Login):
    ret = db.query(classes.Article.picture).filter(classes.Article.id == id and classes.Article.user == user.id).first()
    return base64.b64encode(ret.picture)

# Gets the user's event
def get_Event(db: Session, event_id: uuid, user: classes.Login):
    return db.query(classes.Event).filter(classes.Event.id == event_id).first()

# Gets all article objects of the user
def get_Events(db: Session, user: classes.Login):
    return db.query(classes.Event).all()

# Delets a user's article
def delete_Event(db: Session, event_id: uuid, user: classes.Login):
    event = db.query(classes.Event).filter(
        classes.Event.id == event_id and classes.Event.user == user.id
    )
    return db.delete(event)

# Creates an event with some fields
def create_Event(db: Session, event: schemas.EventBase, user: classes.Login):
    id = uuid.uuid4()
    newEvent = classes.Event()
    newEvent.id = id
    newEvent.name = event.name
    newEvent.location = event.location
    newEvent.eventDate = event.eventDate
    newEvent.isactive = True
    newEvent.user = user.id
    db.add(newEvent)
    db.commit()
    return id

# Updatet ein Event
def update_Event(db: Session,event_id: uuid.UUID, event: schemas.EventBase, user: classes.Login):
    # updatetShow = get_Show(db, show.id)
    editEvent = (
        db.query(classes.Event)
        .filter(classes.Event.id == event_id and classes.Event.user == user.id)
        .first()
    )
    editEvent.name = event.name
    editEvent.location = event.location
    editEvent.eventDate = event.eventDate
    editEvent.tip = editEvent.tip
    editEvent.dept = editEvent.dept
    editEvent.revenue = editEvent.revenue
    editEvent.notes = ""
    for n in editEvent.notes:
        editEvent.notes+=n
        editEvent.notes+='\n'

    editEvent.isactive = editEvent.isactive
    db.merge(editEvent)
    db.commit()
    return editEvent

# Ruft Liste aller Events des Nutzers ab
def get_all_Events(db: Session, user: classes.Login):
    return db.query(classes.Event).filter(classes.Event.user == user.id).all()

# Ruft ein Event des Nutzers ab
def get_Event(db: Session, event_id: uuid, user: classes.Login):
    return (
        db.query(classes.Event)
        .filter(classes.Event.id == event_id and classes.Event.user == user.id)
        .first()
    )

# Ruft das Aktive Event des Nutzers ab
def get_Active_Event(db: Session, user: classes.Login):
    return db.query(classes.Event).filter(classes.Event.user == user.id).filter(classes.Event.isactive == True).first()

# Fügt die Notis den Notizen des Events hinzu
def write_Note(event_id: uuid, note: str, db: Session, user: classes.Login):
    event = db.query(classes.Event).filter(classes.Event.id == event_id).filter(classes.Event.user == user.id).first()
    if event.notes == None:
        event.notes = note
        event.notes +='\n'
    else:
        event.notes += note
        event.notes +='\n'
    db.merge(event)
    db.commit()
    return event
    
#Erstellt einen SizeTable größen eintrag des Nutzers
def create_Size(
    db: Session, size: schemas.SizeBase, article_id: uuid, user: classes.Login
):
    article = get_Article(db, article_id, user)
    if article:
        newSize = classes.SizeTable()
        newSize.id = uuid.uuid4()
        newSize.articleId = article_id
        newSize.size = size.size
        newSize.amount = size.amount
        newSize.event_id = size.event_id
        db.add(newSize)
        db.commit()
        return newSize
    return {}

# Löscht einen SizeTable eintrag über die ID
def delete_Size(db: Session, size_id: uuid, user: classes.Login):
    size = db.query(classes.SizeTable).filter(classes.SizeTable.id == size_id).first()
    if size:
        article = (
            db.query(classes.Article)
            .filter(classes.Article.id == size.articleId)
            .first()
        )
        if article and article.user == user.id:
            db.delete(size)

    return {}

# Läd Artikel mit den Größeneinträgen die zum entsprechenden Event gehören
# Sortiert die Ergebnisse erst nach Artikel und dann nach Größen
def get_Event_Merch(db: Session, event_id: uuid.UUID, user: classes.Login):
    return (
        db.query(classes.SizeTable.articleId, classes.SizeTable.id , classes.SizeTable.size,classes.SizeTable.amount, classes.Article.name, classes.Article.color, classes.Article.price)
        .join(classes.Article)
        .filter(classes.SizeTable.event_id == event_id and classes.Article.user == user.id)
        .order_by(classes.SizeTable.articleId ,asc(classes.SizeTable.size))
        .all()
    )

# Läd Artikel mit den Größeneinträgen die zum Basisbestand gehören (keine Event_id)
# Sortiert die Ergebnisse erst nach Artikel und dann nach Größen
def get_All_Merch(db: Session, user: classes.Login):
    return (
        db.query(classes.SizeTable.articleId, classes.SizeTable.id , classes.SizeTable.size,classes.SizeTable.amount, classes.Article.name, classes.Article.color, classes.Article.price)
        .join(classes.Article) 
        .filter(classes.SizeTable.event_id == None).filter(classes.Article.user == user.id)
        .order_by(classes.SizeTable.articleId , asc(classes.SizeTable.size))
        .all()
    )

# Erstellt Artikel bezogene Daten
# Wird beim erstellen eines Artikels aufgerufen
def create_Data(
    db: Session, articleId: uuid, data: schemas.MerchData, user: classes.Login
):
    article = get_Article(db, articleId, user)
    if article:
        merch_data = classes.MerchData()
        merch_data.articleId = articleId
        merch_data.orgAmount = data.orgAmount
        merch_data.earnings = data.earnings
        merch_data.purchasePrice = data.purchasePrice
        merch_data.purchaseDate = data.purchaseDate
        db.add(merch_data)
        db.commit()
        return merch_data
    return {}

# Updatet die Artikel bezogenen Daten
def update_Data(
    db: Session, articleId: uuid, data: schemas.MerchData, user: classes.Login
):
    article = get_Article(db, articleId, user)
    if article:
        merch_data = (
            db.query(classes.MerchData)
            .filter(classes.MerchData.articleId == articleId)
            .first()
        )
        merch_data.orgAmount = data.orgAmount
        merch_data.earnings = data.earnings
        merch_data.purchasePrice = data.purchasePrice
        merch_data.purchaseDate = data.purchaseDate
        db.merge(merch_data)
        db.commit()
        return merch_data
    return {}

# Artikel Größe wird verkauft/ gelöscht
# Wenn der Artikel zu einem Event gehört wird ebenfalls der Hauptbestand angepasst
# Einnahmen werden bei dem Artikel und dem Event aktualisiert
# Falls newPrice gesetzt ist wird der neue Preis verbucht
def sell_Article(
    db: Session,
    size: schemas.Size,
    user: classes.Login,
    newPrice: Optional[float],
):
    print("newPrice")
    print(newPrice)
    articles = (
        db.query(classes.SizeTable)
        .join(classes.Article)
        .filter(classes.SizeTable.articleId == size.articleId)
        .filter(classes.SizeTable.size == size.size)
        .filter(classes.Article.user == user.id)
    )
    if size.event_id != None and size.event_id != null:
        articleSize = articles.filter(
            classes.SizeTable.event_id == size.event_id
        ).first()
        if articleSize.amount < 2:
            db.delete(articleSize)
        else:
            articleSize.amount = articleSize.amount - 1
            db.merge(articleSize)
        event = db.query(classes.Event).filter(classes.Event.user == user.id).filter(classes.Event.id == size.event_id).first()
        if newPrice != None and newPrice != null:
            event.revenue = event.revenue + newPrice
        else:
            article = (
                db.query(classes.Article)
                .filter(classes.Article.id == size.articleId)
                .first()
            )
            event.revenue = event.revenue + article.price
        db.merge(event)
    mainStock = articles.filter(classes.SizeTable.event_id == None).first()
    merchData = (
        db.query(classes.MerchData)
        .filter(classes.MerchData.articleId == size.articleId)
        .first()
    )
    if newPrice != None and newPrice != null:
        merchData.earnings = merchData.earnings + newPrice
    else:
        article = (
            db.query(classes.Article)
            .filter(classes.Article.id == size.articleId)
            .first()
        )
        merchData.earnings = merchData.earnings + article.price
    db.merge(merchData)
    if mainStock.amount < 2:
        db.delete(mainStock)
    else:
        mainStock.amount = mainStock.amount - 1
        db.merge(mainStock)
    db.commit()
    return articles.all()

# Event wird Archiviert/ Inaktiv gesetzt
# SizeTable einträge zum Event werden gelöscht
def arcive_Event(db: Session, event_id: uuid.UUID, user: classes.Login):
    deleted = db.query(classes.SizeTable).join(classes.Event).filter(classes.SizeTable.event_id == event_id).filter(classes.Event.user == user.id).all()
    if db.query(classes.Event).filter(classes.Event.user == user.id and classes.Event.id == event_id).first() != null:
        event = db.query(classes.Event).filter(classes.Event.id == event_id).first()
        event.isactive = False
        db.merge(event)
        if(len(deleted) > 0):
            statement = delete(classes.SizeTable).where(classes.SizeTable.event_id == event_id)
            db.execute(statement)
            db.commit()
        return deleted
    return {}

# Erstellt aus sizes die entsprechenden Einträge in der SizeTable
# oder Updatet die entsprechenden Größen falls vorhanden
def create_Event_Sizes(db: Session, sizes: List[schemas.SizeBulk]):
    index = 0
    while(index < len(sizes)):
        updateSize = (db.query(classes.SizeTable).filter(classes.SizeTable.articleId == sizes[index].articleId)
        .filter(classes.SizeTable.event_id == sizes[index].event_id)
        .filter(classes.SizeTable.size == sizes[index].size).all()
        )
        if len(updateSize) < 1:
            newSize = classes.SizeTable()
            id = uuid.uuid4()
            newSize.id = id
            newSize.articleId = sizes[index].articleId
            newSize.event_id = sizes[index].event_id
            newSize.amount = sizes[index].amount
            newSize.size = sizes[index].size
            db.add(newSize)
        else:
            updatetSize = updateSize[0]
            updatetSize.amount = sizes[index].amount
            db.merge(updatetSize)
        index = index + 1
    db.commit()
    return sizes

# Läd die Größeneinträge des Artikels aus dem Basisbestand (ohne Event_id)
def get_Article_Sizes(db: Session, article_id):
    return db.query(classes.SizeTable).filter(classes.SizeTable.articleId == article_id).filter(classes.SizeTable.event_id == None).all()
