from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    Float,
    DATE,
    LargeBinary,
    Enum,
)
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column
import enum

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import UniqueConstraint

Base = declarative_base()

# Geplante N/M Beziehnung von Artikeln und Events (nicht genutzt)
class EventMerch(Base):
    __tablename__ = "EventMerch"

    article_id = Column(UUID, ForeignKey("Article.id"), primary_key=True)
    event_id = Column(UUID, ForeignKey("Event.id"), primary_key=True)

# Größen Enumeration
class Sizes(enum.Enum):
    XS = 1
    S = 2
    M = 3
    L = 4
    XL = 5
    XXL = 6
    XXXL = 7
    OS = 8

#Artikel tabelle mit Foreign Key von user(Login Tabelle) und bezieung zur GrößenTabelle (SizeTable) 
class Article(Base):
    __tablename__ = "Article"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True)
    name = Column(String, unique=False, index=True)
    picture = Column(LargeBinary, default=None)
    price = Column(Float)
    color = Column(String)

    user = Column(UUID(as_uuid=True), ForeignKey("Login.id"))
    events = relationship("Event", secondary="EventMerch")

#MerchDaten Tabelle mit genaueren Infos zum Artikel 
class MerchData(Base):
    __tablename__ = "MerchData"

    articleId = Column(
        UUID(as_uuid=True), ForeignKey("Article.id"), primary_key=True, index=True
    )
    orgAmount = Column(Integer, index=True)
    earnings = Column(Float, default=0)
    purchasePrice = Column(Float, nullable=True)
    purchaseDate = Column(DATE, nullable=True)

    # owner = relationship("User", back_populates="items")

#GrößenTabelle mit Event ID als Foreign Key
class SizeTable(Base):
    __tablename__ = "SizeTable"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True)
    articleId = Column(UUID(as_uuid=True), ForeignKey("Article.id"), index=True)
    size = Column(Enum(Sizes))
    amount = Column(Integer)
    event_id = Column(
        UUID(as_uuid=True), ForeignKey("Event.id"), default=None, nullable=True
    )

#Event Tabelle mit User (Login) Foreign Key
class Event(Base):
    __tablename__ = "Event"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True)
    name = Column(String)
    location = Column(String, nullable=True)
    eventDate = Column(DATE)
    tip = Column(Float, nullable=True, default=0)
    dept = Column(Float, nullable=True, default=0)
    revenue = Column(Float, default=0, nullable=True)
    isactive = Column(Boolean, default=False, nullable=False)
    notes = Column(String, nullable=True)
    user = Column(UUID(as_uuid=True), ForeignKey("Login.id"))

    articles = relationship(Article, secondary="EventMerch", viewonly=True)

# Nutzerverwaltungs tabelle
class Login(Base):
    __tablename__ = "Login"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True)
    username = Column(String, unique=True, nullable=False)
    pw_hash = Column(String, nullable=False)
