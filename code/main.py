from typing import List, Optional

from sqlalchemy import Float

from database import Database

from schemas import EventBase, ArticleBase, Article, SizeBase, Size, MerchData, SizeBulk

from fastapi import FastAPI, Depends
from fastapi_login import LoginManager
from fastapi.security import OAuth2PasswordRequestForm

from sqlalchemy.orm import Session
from fastapi.middleware.cors import CORSMiddleware

import uuid
import crud
import auth
import classes

# change this
SECRET = "bc131d8af5365c66a1964222477a4d2736f2a3b57cc8bcc1"

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:8080/home",
    "http://localhost:8080/",
    "http://localhost:8080/event/purchase",
    "http://kalsary.de",
    "http://www.kalsary.de",
    "https://kalsary.de",
    "https://www.kalsary.de"

]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

database = Database()
database.init_db()

login_man = LoginManager(SECRET, token_url="/api/auth", use_cookie=True)


@login_man.user_loader()
def load_user(username: str):
    return (
        database.session.query(classes.Login)
        .filter(classes.Login.username == username)
        .first()
        
    )


@app.post("/api/auth/new/")
def new_login(username: str, password: str):
    return auth.create_login(database.session, username, password)


# https://github.com/MushroomMaula/fastapi_login
@app.post("/api/auth/")
def login(data: OAuth2PasswordRequestForm = Depends()):
    return auth.login_fn(database.session, login_man, data)


# Get All Articles
@app.get("/api/article/all/")
def get_all_articles(user=Depends(login_man)):
    return crud.get_Articles(database.session, user)

# Get Image from Article
@app.get("/api/article/image/{id}")
def get_all_articles(id: uuid.UUID, user=Depends(login_man)):
    return crud.get_Image_Article(database.session, id, user)

# Create Article
@app.post("/api/article/")
def post_article(article: ArticleBase, user=Depends(login_man)):
    return {"id": crud.create_Article(database.session, article, user)}

# Update Article Image
#@app.put("/api/article/{id}")
#def put_article_image(id: uuid.UUID, image: UploadFile, user=Depends(login_man)):
#    return crud.update_Article_image(database.session, id, image.file.read(), user)

# Update Article
@app.put("/api/article/")
def update_article(article: Article, user=Depends(login_man)):
    return crud.update_Article(database.session, article, user)


# Get Article with ID
@app.get("/api/article/{art_id}")
def get_article(art_id: uuid.UUID, user=Depends(login_man)):
    return crud.get_Article(database.session, art_id, user)


# Delete a single Article with ID
@app.delete("/api/article/{art_id}")
def delete_article(art_id: uuid.UUID, user=Depends(login_man)):
    return crud.delete_Article(database.session, art_id, user)

@app.get("/api/article/size/{article_id}")
def get_Article_Sizes(article_id: uuid.UUID):
    return crud.get_Article_Sizes(database.session, article_id)

# Create Event
@app.post("/api/event/")
def post_Event(event: EventBase, user=Depends(login_man)):
    return {"id": crud.create_Event(database.session, event, user)}


# Get All Events
@app.get("/api/event/all/")
def get_all_Events(user=Depends(login_man)):
    return crud.get_all_Events(database.session, user)


# Get Event with ID
@app.get("/api/event/{event_id}")
def get_Event(event_id: uuid.UUID, user=Depends(login_man)):
    return crud.get_Event(database.session, event_id, user)


# Update Event with ID
@app.put("/api/event/{event_id}")
def update_Event(event_id: uuid.UUID, event: EventBase, user=Depends(login_man)):
    return crud.update_Event(database.session,event_id, event, user)


# Delete Event
@app.delete("/api/event/{event_id}")
def delete_Event(event_id: uuid.UUID, user=Depends(login_man)):
    crud.delete_Event(database.session, event_id, user)


# Create Size
@app.post("/api/merch/{articleId}")
def create_Size(size: SizeBase, articleId: uuid.UUID, user=Depends(login_man)):
    new = crud.create_Size(database.session, size, articleId, user)
    return new

# Get All Merch
@app.get("/api/merch/all/")
def get_All(user=Depends(login_man)):
    return crud.get_All_Merch(database.session, user)

# Get Merch from Event
@app.get("/api/merch/{event_id}")
def get_Event_Merch(event_id: uuid.UUID, user=Depends(login_man)):
    return crud.get_Event_Merch(database.session, event_id, user)


# Delete Size
@app.delete("/api/merch/{articleId}/{sizeId}")
def delete_Size(size_id: uuid.UUID, articleId: uuid.UUID, user=Depends(login_man)):
    return crud.delete_Size(database.session, size_id, articleId, user)




# Create new Merch for Article with ID
@app.put("/api/article/data/{articleId}")
def update_Data(articleId: uuid.UUID, data: MerchData, user=Depends(login_man)):
    return crud.update_Data(database.session, articleId, data, user)

# Verkauf einer Artikelgröße
@app.delete("/api/sell/")
def sell_Article(size: Size, newPrice: Optional[float] = None, user=Depends(login_man)):
    print(newPrice)
    if newPrice:
        return crud.sell_Article(database.session, size, user, newPrice)
    return crud.sell_Article(database.session, size, user, None)

# Archivieren eines Events (setzt es Inaktiv und Löscht Eventbezogene SizeTable einträge)
@app.delete("/api/event/arcive/{event_id}")
def arcive_Event(event_id: uuid.UUID, user=Depends(login_man)):
    return crud.arcive_Event(database.session, event_id, user)

# Erstellt ein bis mehrere Artikel Größen oder Update die entsprechende Größe
@app.post("/api/event/sizes/")
def create_Event_Sizes(sizes: List[SizeBulk]):
    return crud.create_Event_Sizes(database.session, sizes)

# Fügt dem Aktiven Event eine Notiz hinzu
@app.post("/api/event/note/{event_id}")
def addNote(event_id: uuid.UUID, note: str, user=Depends(login_man)):
    return crud.write_Note(event_id, note, database.session, user)

# Läd das Aktive Event des Nutzers
@app.get("/api/event/active/")
def get_Active_Event(user=Depends(login_man)):
    return crud.get_Active_Event(database.session, user)
