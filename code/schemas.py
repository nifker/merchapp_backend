from datetime import date
from typing import List, Optional
import uuid

from pydantic import ArbitraryTypeError, BaseModel
from sqlalchemy import LargeBinary, null

from classes import Sizes

# Schematas für die wichtigen Daten die der Client an den Server übergibt
# IDs und ähnliche Felder werden ausgelassen
# Basismodell zum Erstellen
# Hauptmodelle erben von BasisModelln und werden zum Abrufen/Updaten oder Löschen genutzt

class ArticleBase(BaseModel):
    name: str
    price: float
    color: str

class Article(BaseModel):
    articleId: uuid.UUID
    name: str
    price: float
    color: str

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True


class MerchData(BaseModel):
    orgAmount: Optional[int] = None
    earnings: Optional[float] = None
    purchasePrice: Optional[float] = None
    purchaseDate: Optional[date] = None


class EventBase(BaseModel):
    name: str
    location: str
    eventDate: date


class EventUpdate(EventBase):
    id: uuid.UUID
    tip: float
    dept: Optional[float] = 0
    isactive: bool
    notes: List[str]
    revenue: float


class SizeBase(BaseModel):
    size: Sizes
    amount: int
    event_id: Optional[uuid.UUID] = None


class SizeBulk(SizeBase):
    articleId: uuid.UUID
    


class Size(SizeBulk):
    id: uuid.UUID

    class Config:
        orm_mode = True
