from fastapi import Depends
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_login.exceptions import InvalidCredentialsException
from fastapi_login import LoginManager

from sqlalchemy.orm import Session

from argon2 import PasswordHasher

from classes import Login

import uuid

import main


# Creates a login in the database from username and password and hashes the password
def create_login(db: Session, username: str, password: str):
    user = db.query(Login).filter(Login.username == username).first()
    if not user:
        id = uuid.uuid4()
        hasher = PasswordHasher()
        hash = hasher.hash(password)

        login = Login()
        login.id = id
        login.username = username
        login.pw_hash = hash

        db.add(login)
        db.commit()
    else:
        # TODO
        raise InvalidCredentialsException


# Creates a login session for the user if the credentials are valid
def login_fn(
    db: Session, manager: LoginManager, data: OAuth2PasswordRequestForm = Depends()
):
    username = data.username
    password = data.password

    hasher = PasswordHasher()

    login_user = main.load_user(username)
    if not login_user:
        raise InvalidCredentialsException  # you can also use your own HTTPException
    elif not hasher.verify(login_user.pw_hash, password):
        raise InvalidCredentialsException

    access_token = manager.create_access_token(data=dict(sub=username))
    return {"access_token": access_token, "token_type": "bearer"}
