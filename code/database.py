from enum import Enum

from configparser import ConfigParser

from sqlalchemy import create_engine

from classes import Base

import psycopg2

from sqlalchemy.orm import sessionmaker

# Handles all databases connections and the current session
class Database:
    # https://www.postgresqltutorial.com/postgresql-python/connect/
    def __init__(self):
        section = "postgresql"
        filename = "database.ini"
        # create a parser
        parser = ConfigParser()
        # read config file
        parser.read(filename)

        # get section, default to postgresql
        db = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            raise Exception(
                "Section {0} not found in the {1} file".format(section, filename)
            )

        self.engine = create_engine(
            "postgresql+psycopg2://"
            + db["user"]
            + ":"
            + db["password"]
            + "@"
            + db["host"]
            + "/"
            + db["database"]
        )

        self.db = psycopg2.connect(**db)

        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def init_db(self):
        Base.metadata.create_all(self.engine)
